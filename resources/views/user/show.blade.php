@extends('parts.header')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                  <h3 class="card-title">{{ $user->name }} - {{ $user->firstname }} {{ $user->lastname }}</h3>
                </div>
    
                <div class="card-body p-0 px-3">
                    <div class="tab-content">                    
                        <div class="card-body px-3 pt-1 pb-0 mb-0">
                            <div class="row">
                                <div class="col">
                                    <legend class="pt-5 pb-3">Gebruiker</legend>

                                    <div class="form-group">
                                        <label class="control-label required">{{ trans('attribute.user.name') }}</label>
                                        <input class="form-control" minlength="2" maxlength="32" value="{{ $user->name }}" required disabled>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label required">{{ trans('attribute.user.email') }}</label>
                                        <input class="form-control" minlength="2" maxlength="32" value="{{ $user->email }}" required disabled>
                                    </div>
                                </div>
                                
                                <div class="col">
                                    <legend class="pt-5 pb-3">Persoonlijk</legend>
                                    
                                    <div class="form-group">
                                         <div class="form-group">
                                            <label class="control-label required">{{ trans('attribute.user.firstname') }}</label>
                                            <input class="form-control" minlength="2" maxlength="32" value="{{ $user->firstname }}" required disabled>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                         <div class="form-group">
                                            <label class="control-label required">{{ trans('attribute.user.lastname') }}</label>
                                            <input class="form-control" minlength="2" maxlength="32" value="{{ $user->lastname }}" required disabled>
                                        </div>
                                    </div>
                                </div>

                                <div class="col">
                                    <legend class="pt-5 pb-3">Rechten</legend>
                                    
                                    <div class="table-responsive">
                                        <table class="table table-outline table-vcenter text-nowrap card-table">
                                            <tbody>
                                                @foreach($permissions as $permission)
                                                    <tr>
                                                        <td>{{ str_replace('.', ' ', $permission->name) }}</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="col">
                                    <legend class="pt-5 pb-3">Social media</legend>
                                    
                                    <div class="form-group">
                                        <a href="https://twitter.com/{{ $user->social_twitter }}" target="_blank" class="btn btn-azure btn-block @if($user->social_twitter === '#') disabled @endif">Twitter</a>
                                    </div>
                                                    
                                    <div class="form-group">
                                        <a href="https://facebook.com/{{ $user->social_facebook }}" target="_blank" class="btn btn-blue btn-block @if($user->social_facebook === '#') disabled @endif">Facebook</a>
                                    </div>

                                    <div class="form-group">
                                        <a href="https://instagram.com/{{ $user->social_instagram }}" target="_blank" class="btn btn-purple btn-block @if($user->social_instagram === '#') disabled @endif">Instagram</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="card-footer text-right">
                            @can('edit.user')
                                <a href="{{ route('user.edit', [$user->id]) }}" class="btn btn-secondary">Bewerk gebruiker</a>
                            @endcan
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
@endsection