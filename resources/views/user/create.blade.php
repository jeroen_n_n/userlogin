@extends('parts.header')

@section('content')
    @can('create.user')
        <div class="row">
            <div class="col">
                @if($errors->first())
                    <div class="alert alert-icon alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert"></button>
                        <i class="fe fe-alert-triangle mr-2" aria-hidden="true"></i> {{ $errors->first() }}
                    </div>
                @endif

                @if (Session::has('success'))
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert"></button>
                        {{ Session::get('success') }}
                    </div>
                @endif
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Maak een nieuw account</h3>
                    </div>

                    <div class="card-body p-0 px-3">
                        <form action="{{ route('user.store') }}" method="post">
                            @method('post')
                            @csrf
                                
                            <div class="tab-content">
                                <div class="tab-pane px-3 pb-3 active show" id="account" role="tabpanel" aria-labelledby="account-tab">
                                    <div class="row">
                                        <div class="col-4">
                                            <legend class="pt-5 pb-3">Account</legend>
                                            
                                            <div class="form-group">
                                                <label class="control-label required">{{ trans('attribute.user.name') }}</label>
                                                <input type="text" name="name" class="form-control @if($errors->has('name')) is-invalid @endif" minlength="2" maxlength="32">
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label required">{{ trans('attribute.user.email') }}</label>
                                                <input type="email" name="email" class="form-control @if($errors->has('email')) is-invalid @endif" minlength="2" maxlength="32" autocomplete="off" required>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label required">{{ trans('attribute.user.firstname') }}</label>
                                                <input type="text" name="firstname" class="form-control @if($errors->has('firstname')) is-invalid @endif" minlength="2" maxlength="32" autocomplete="off" required>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label required">{{ trans('attribute.user.lastname') }}</label>
                                                <input type="text" name="lastname" class="form-control @if($errors->has('lastname')) is-invalid @endif" minlength="2" maxlength="32" autocomplete="off" required>
                                            </div>
                                        </div>
                                        
                                        <div class="col-4">
                                            <legend class="pt-5 pb-3">Wachtwoord</legend>

                                            <div class="form-group">
                                                <label class="form-label required">{{ trans('attribute.user.password')}}</label>
                                                <input type="password" name="password" class="form-control @if($errors->has('password')) is-invalid @endif" minlength="2" maxlength="32" autocomplete="off" required/>
                                            </div>
                
                                            <div class="form-group">
                                                <label class="form-label required">{{ trans('attribute.user.password_confirmation')}}</label>
                                                <input type="password" name="password_confirmation" class="form-control @if($errors->has('password_confirmation')) is-invalid @endif"  minlength="2" maxlength="32" autocomplete="off" required/>
                                            </div>
                                        </div>

                                        <div class="col-4">
                                            <legend class="pt-5 pb-3">Social media</legend>

                                            <div class="form-group">
                                                <label class="control-label">{{ trans('attribute.user.social_twitter') }}</label>
                                                <input type="text" name="social_twitter" class="form-control @if($errors->has('social_twitter')) is-invalid @endif" minlength="1" maxlength="32" placeholder="https://twitter.com/" autocomplete="off" required>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label">{{ trans('attribute.user.social_facebook') }}</label>
                                                <input type="text" name="social_facebook" class="form-control @if($errors->has('social_facebook')) is-invalid @endif" minlength="1" maxlength="32" autocomplete="off" required>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label">{{ trans('attribute.user.social_instagram') }}</label>
                                                <input type="text" name="social_instagram" class="form-control @if($errors->has('social_instagram')) is-invalid @endif" minlength="1" maxlength="32" autocomplete="off" required>
                                            </div>

                                            <div class="form-group">                                        
                                                <div class="alert alert-warning alert-dismissible">
                                                    <button type="button" class="close" data-dismiss="alert"></button>
                                                    Wilt u geen social media gebruiken? Gebruik dan <b>#</b>.
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="card-footer text-right">
                                        <button class="btn btn-primary" type="submit">Maak account</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @else
        <div class="alert alert-warning alert-dismissible">
            <button type="button" class="close" data-dismiss="alert"></button>
            Je hebt geen toegang tot deze pagina.
        </div>
    @endcan
@endsection