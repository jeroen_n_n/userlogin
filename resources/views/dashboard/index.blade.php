@extends('parts.header')

@section('content')

    <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert"></button>
        Hallo {{ Auth::User()->name }} - Welkom op het Emprise systeem.
    </div>

    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body p-3 text-center">                    
                    <div class="h1 m-0">{{ $administratorsCount }}</div>
                    <div class="text-muted mb-4">Rol: Administrator</div>
                </div>
            </div>
        </div>

        <div class="col">
            <div class="card">
                <div class="card-body p-3 text-center">                    
                    <div class="h1 m-0">{{ $usersCount }}</div>
                    <div class="text-muted mb-4">Rol: User</div>
                </div>
            </div>
        </div>

        <div class="col">
            <div class="card">
                <div class="card-body p-3 text-center">                    
                    <div class="h1 m-0">{{ $users }}</div>
                    <div class="text-muted mb-4">Aantal gebruikers</div>
                </div>
            </div>
        </div>

        <div class="col">
            <div class="card">
                <div class="card-body p-3 text-center">                    
                    <div class="h1 m-0">{{ $usersNotActive }}</div>
                    <div class="text-muted mb-4">Aantal niet actieve gebruikers</div>
                </div>
            </div>
        </div>
    </div>

@endsection