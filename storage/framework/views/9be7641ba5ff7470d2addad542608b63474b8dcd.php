<!DOCTYPE html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <title><?php echo e(config('app.name')); ?> - <?php echo $__env->yieldContent('pageTitle'); ?></title>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>


    <script src="<?php echo e(asset('js/require.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/dashboard.js')); ?>"></script>
    <link href="<?php echo e(asset('css/app.css')); ?>" rel="stylesheet">

    <script>
        requirejs.config({
            baseUrl: '././'
        });
    </script>
    
    <link href="<?php echo e(asset('plugins/charts-c3/plugin.css')); ?>" rel="stylesheet" />
    <script src="<?php echo e(asset('plugins/charts-c3/plugin.js')); ?>"></script>

    <link href="<?php echo e(asset('plugins/maps-google/plugin.css')); ?>" rel="stylesheet" />
    <script src="<?php echo e(asset('plugins/maps-google/plugin.js')); ?>"></script>

    <script src="<?php echo e(asset('plugins/input-mask/plugin.js')); ?>"></script>
</head>
<body>
    <div class="page">
        <div class="page-main">
            <div class="header py-4">
                <div class="container">
                    <div class="d-flex">
                        <a class="header-brand" href="<?php echo e(url('/')); ?>/admin/dashboard">
                            <b>Emp</b>rise
                        </a>
                        
                        
                        <div class="d-flex order-lg-2 ml-auto">

<div class="dropdown-divider"></div>



    <div class="dropdown">
      <a href="javascript:void(0);" class="nav-link pr-0 leading-none" data-toggle="dropdown" data-target="#">
        <span class="avatar" style="background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQAAAAEACAYAAABccqhmAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAWyElEQVR4nO3deZBcR2HH8e9e2ksraaVdybpty5a1lmXhiyNgDmOcFOEwxoBjE5IAOYBwJQEKCCGYIxBCpagk2DicNnFIMOZIMCYcTuwQgw+MbB1Y9hrJRqtjde6tPWbyR3vQejWzOzPd773u6d+nagtXoen39u3073X369ddl8/nkWgV/vh1mZ6FZKYx6xMQay4S3KYMhUfAFADh8LWpVuq8FAwBUAD4ydfKXoliv4NCwTMKAD9kU+HvbDH/+9yxtI448/dUIGSsToOAmXB30QuVOCtuw0OBkDIFQLqqv9hZV/RK2QWDgiAlCoBkVXdxQ6vs5VIoeEcB4F7lF7RWK/xcqgsEBYFDCgB3yr+QsVb4uVQWCAoCBxQA9lTxXVMQpEYBUDlV+CyUHwoKhAooAMpX3oVSpU+WgsApBcDcVPF9pCBwQgFQmip+CBQEVhQAJ5v9gqjC+23uQFAQTKMAOEEVv5YoCMqiADBKXwRV/LApCGalADBOvgiq+LWldBAoACKmih+b4kEQbQjEGgCq+LFTEADxLQgSZdpJ2aJbJDWWFoBG+KW42QcJaz4IYggAjfDL7CJ+UhBnAKjiSzERPimo5QBQxZfqRDRAWIsBoIovbkQQBPVZn4BjNZdm4p08NfQ9q6UWQPFfRHd/sVXDYwO1EgBq9kuyajQEaiEAnvoLqOJLkmpsXCD0MYDg00sCU/wGE+z3MNQAKD4Qo7u/pKGGQiDEANBgn2SvdAgEFQShBYAqv/ij9PcumBAIKQBU+cU/gYdACAFwcrOqcNFV+cUHAYeA748BddeXsAQ2XyCEFsBTqfKLzwL7fvoaAHrMJ+EK6DGhjwGg/r6EL5AQ8DEATqbKLyEKIAR8GwTUvH6pTScPDnoxKBhGC0BEEuFLAJR+1i9SC07+PnvR9PYhADTaL3HwMAR8CICnUuWXuGQaAlkPAqrZL3HyZFAwyxZA5s0fEY9kUh+yCgD1+yVunswR8GMMQJVfYuTB9z7tANDjPpHpMn4ykGYAqNkvUkyGIZBdF0CVXyRzaQWAmv0isyneCki8JeDHIKCIZCLpiUDq94tUKsVJQkkGgJr9ItVKKQTUBRCJWFIBoLu/iI2UHg2qBSASsSQCQHd/ERdSaAWoBSASMdcBoLu/iEsJTxBSC0AkYq7mAWjCj0jSEpgb4CIA1OwXSYvjEFAXQCRitgGgu79Imhw/GlQLQCRiNgGgu79IFhy2AtQCEIlYtQGgu79Ilhy1AtQCEIlYNQGgu7+IDxy0AtQCEIlYpQGgu7+ITyxbAWoBiERMASASMQWASMQqCQD1/0V8ZDEOoBaASMTKDQDd/UV8VmUrQC0AkYiVEwC6+4uEoIpWgFoAIhFTAIhEbK4AUPNfJCQVdgPUAhCJmAJAJGKNWZ9A8JqWQccF0L4R2npg3nJoXg5NXVDfYn4AcmPmZ/IojO+H8QMw1gvDO2BkBwxtAaYy/VWcmLcSFlwELeugZTU0r4bmNdDY8eT1aIOGNsjnIDcCUyNPXpcjMLYLjj8BY0+YazJ4P0wdy/o3qmmzbQyi/n8pC54J3a+GzkugbYObMnNjMPgAHLsLDt8OA3eT0JbwbjWvhiUvM9ei4yKYt9Rd2fkcjD4Kg/fBoe+Y65Ibdld+LStzAxEFQLnqW2H5G2HlW6Dl1OSPNzkAh2+Dg9+EQ9+F/PHkj1muxk5zLbqvhPmb0ztubhyO3gH7vwL9t1ITLaakWAaAKv+v1cGKP4G1H4CmxdmcwtSQCYJ9N8KxO7M5BzBdnFVvh6VXnejaZOX4r2DPdbD3c+omlFJGCCgAZtN6FvTcmO5dbi5ju6Dvn2Hfl2DyUDrHbOqG0z4My14HdZ6NG08chl0fgr03EESXKU0KAAvdr4H115kBKx/lxuG+88xAYpJWvtW0fhoXJHscW8PbYOebYPCerM/EH2UEgGdx7omVb4WeL/tb+QFGHk628jcugXO+Bes+6X/lB/MU5mk/gpVvz/pMglIsAOK++694k/nS+67v+uTK7ng6XHAvLP7N5I6RhLpGWPcJOPtrZtA2dmXMClQLYLrOS8Oo/JMDcODmZMpe9Hw49zZoXpFM+Wnoeilsug0aOrI+E+8pAAoaO+GsL5i7iO/2fwVyo+7LXfxiOOeb0DDffdlpW/gsOPd2aFiY9Zl4TQFQsO6TbiexJKnvs+7L7LgQzr45+8d7LnVcABtvARqyPhNvzQyAOPv/rWfB0quzPovyHLkDRh92W+a8laai1FLlL1h0MZz56azPIjtzjAOoBQCw5j3+Pd8uZa/ru3+DqfzzTnFcrkeWvxFOeUPWZ+GlADq8Catvh67L3ZVXmM8/tMW84DI1YPrUDQvNTMKmbmjfZCYXtayprOzjfXDwW+7OFWDte6HjPLdlTjf6GAz8H4z2mv+eOmZeAKqrNy8GNS2BltPMLMOFz06uG7buE+ZdgvE9yZQfKAXAkpe4ed5/5Ifw2Hth+MHyP9PYaV6g6bwUFl8294tFez+P09lubWfD6ne7K69g9FHYd5N5UnH8ico+234unPI60yVzOfW6YT6s/wxsfbm7MmvAzJmA8Y0BrL/BfOFs7P6I+bHV1gNdV8DS10Db+qf+f/lJ+Mk6mNhvf5yCzXeY0XJXjveZabn7b8Q6qBo6YNU7YfWfuR2b2H41HLzVXXkhmGVG4PQAiK/yA1y0FVrPqP7zfTfAo29zdz4FHReaufdLrzIz8fpvgR2vdVd+52Ww6dvuyuu/BXa+xf2LOW1nQ89NZqafC8Pb4f7z3ZQVkhIhEHkANMDFg9UPAE4chHt6YGrQ7WlNV99qXrsdfgiGfu6u3KfdZRbucOHxj8Ouv3ZTVjH17XDOrbDoeW7K234NHPy6m7JCUSIAAhn6Tkjr6Xaj//23JFv5wUz42X+T28rfeam7yr/7Y8lWfjCLgDz0Ehj4qZvy1r7PTTk1IO4AaFpi9/mjd7k5j7S5eiTW/3XYfa2bsuaSn4CtV5h1AGy1bzSrOknkAWA75dXlgFxaGjthyW/bl3O8D3a+2b6cSkwegkf+1E1ZyywHfmtE3AFgO++/IYDXZGdaejXUz7Mvp/dd2azEc/h26P+GfTndV0Jds305gSsEQIQDgJgJKTZaTnNzHmlyMelpaEu2g2i7rzULhtpoXACdL3RzPiEoMSU47hbA1IDd57uvdHMeaalvhQXPsC/n8U/Yl2FjZIdZJdhW56X2ZQQu7gAYq3CW2kwLnwULfsPNuaRh0Qvsm//jB9xPR67Gvi/al9F5iX0ZgYs7ACYPmUUlbWz4ktkMJAQuvvD9/44Xy3Ef/q79365tg3kTMmJxBwDA0AN2n29ZA5t/aHbC8d18By/9HPpP+zKcyMOR/7IvpuMC+zICpgA47OBL1Ho6nH83LL3GvqwktW+y+/zUkF9zHw5/z74M22sSOAWAqzta4wLY8Hk49/vmjTbfNK+1X9138H68aP4XDDhYArz9HPsyAqYAGOuFw993V96ii+H8n8CGG6H1THfl2nLxMs3gvfZluDTWazZbteHqJaNAKQAAfvX3bsurq4elr4YLH4CzPufHfIHm1fZlDG+zL8O14R12n690UZYaowAAOPojswGna3WNsOy1cNFDZsXhLFsEzQ6eVIw+Yl+Ga7abo9S3mOnRkVIAFPT+hf3MwFLqGmHZ1XDhFuj5l2z6nfMcrPM/+kv7MlxzcU6hPMZNgAKgYKwXev882WPU1UP3K+GC++Dsf0t3sND2S56fTG8z0kpMHLAvI+RNUCwpAKbb90Wz6UYaul5uBgt7vpLOGEHjIrvP2066ScqEg1CKePMQBcBMD/+xWXs/DXX15n2CC7fA6X+b7FZWtuvqTXoaAC7Oqxb3QyiTAuAkU7DtivRCAMz8/FVvM4OF3a9K6BiWr77mjrs5D9dy4/ZlRB4Acb4KPJvcKGx9mVnyK03zTjELYJ79NWha5rZs2y+5i4qWhLwCoGx3tpz40ZqAc8hPmFV4e99jBsDS1PVSsz23y9dVbRc/SfsalMtJC8DBAimheO5YYYHQPJCvZ9oa4b/+B3LCnk/DA88xi2Ckad5SOOfbsMrRkwnbO6Wvuya7qLy+tm5cK7IysFoA5Rj6OfzsmfDY+80LMWmpq4fTPwpn/qN9WTnLYPf1LlnnIgDivekpAMqWh199yuwDsPcL9ktSVWL5G+HMz9iVYTuIZzuImBQX/XcFgJRtoh8eeTPcdx4c+Gp6QbD89bDmvdV/3vZL3uhwnz6XXEzjzY3alxEoBUC1Rh+GX/w+3LcZ9t+cziDZ2g+YLcOqYTuRx+VGnS41ddmXYftGYcAUALZGH4GHXw/3boK9n0t2QKmuHtZ/Fmio/LPjey2P3QiNlhupJGFet30ZttcmYAoAV8Z+aTatuOcs6Ls+uX5l+0YzaahS4332x2714LXmmWw2di1wsdtQoBQAro3vhUffAfdsNDsHJ9E1WPEmZj69ndNxB3c5H9c9tD2nqZHk93f0mAIgKeN7zLbh9252s5PNdC1rKt/ea8zBa7PzPVw/r+0su8+P7XJyGqFSACRtrBd2/A48+Fsw+qi7cpf9XmX/fvgh+2N2ONpR2JW2Hvt1Doe3ujmXQCkA0nL0v+H+i8yjQxcWPruyfz/Rbzb1sNFxAVUNQCal4+n2ZbgIxoApANKUGzWPDnd/zL6spsXQWmHz1/bL3jDfLHrqi8WX2ZehAJDU7b4W9ljO7ANYUOEd0MWqvotfbF+GEw3Q+SK7IvI5GPBspeOUKQCy0vsuGN5uV0al6/wd+YHd8QCWXoUX3YCulzvo/z/k5zJnKVIAZGYKnvikXRFNFU6DPXa3/ctM85aa15WztvwN9mUccbgfRKAKAaBXgrNw6Da7z1c8P38Kjv6P3TEBVr/Hvgwb88+Hzhfal+OiRRSKIq8Cg1oA2Zo6ZjcPvZo34fpvrf54BR3nQdcV9uVU69QP2ZcxfsCvfQ4zogDIms377NU05w/e6mZNg3V/l81qut2vgcWWg38AB27Gq30OMxJvANS3mp8sNXRAQ1v1n58aqPwzuVE3rYDmFbDewZOMio65Fs5wtI3bvi+7KSdw8QZA6zp45i/h9I+bL1YWFr3A7vNju6v73N4b7I5b0P1KWPtXbsqaS8NC2PQtN68lH/sxjFjuKVgj4g0AMJtlrHoHPH0HbPw6LHkJFb9kY+OUCqfzzlTtXn2D97nbEXnt++DUa92UVUrTMtj8A2jb4Ka83R91U04NmB4A8T4JqKs3L9dsvAWe0Wu+0JXOsqtU1+WVv9AzXT4Hgw9U//ndH67+szOteTf0/Gsym2wuej6c/2N3LyIdu9tsBhuTEk8AIPYWQDHNK8wX+qItcP5PYdU73b8G23U5bPiSXRm2k1gG74FD37E7h+m6X2G2Q1/+hziZKNS8GtbfAJtug+ZV9uUV7Pqgu7JqQF0+/5R9QeLZJKT9HLNJZ7lGdsLh2+HYnaYPOXmk8mO29cDavzR9Z1u7P2amFNtoXm0qbcN8+/OZ7nif2WPxwFdhpILZjvWtZlxk2TWw5KXuVyLedyPs/CO3ZYZglhaAAqBao4/C0EMwss0Mxh3fAxMHzSh7fhIa2qFhgRlsbN9o5q23b3Rz7vmcWZ34eJWDgNOteAuc8Sn7ckqZOAgDP4XRXnOdpgbMIhx1jeYJSFOXuUZtPeZtw6T2HxjfZ9ZmmDqWTPk+myUAPN3tIQCtZzy5HNUr0j/2oe+4qfwAff8EXS+DRc9zU95MTV12Yx0u5HOw881xVv45zBwDiHcgMBT5nPt+7ParYPQxt2X6ZPeH4bDltOtQzXL3Bw0Chqfv+sr61eWYPALbrkx316O09N8Cj/9N1mfhLQVASEZ+AY9ZbA4ya9nbYevltRUCh74Lv/iDrM/CawqAUEwchG2vgrzlFl+zOfa/8OCLa2OjjP5vmFZNfiLrM/FasQDQOIBvJgdg6xXVz/yrxOA9sOWysFfL7bsBdlxN9C/7zNH/B7UA/De+D7ZcYipmWoYfhPufAQf/I71jujA1BDt+1yzHPuOJthSnAPDZ0bvgZ8/OZunqqWOw/VXwyNvD6BIcvQt+9izo/1rWZxKUeANgvB8G78/6LIqbGoHH3g8PvshsMJKlvZ81uxylvSV6ucYeh+3XmGuVRhepxsycCThdHLMCFz7HbLWVxNTTSuUnzU7Duz7o54aVbT2w8q2w7OrqViNyaWQn7PkHM703yYHRUJXR/wcFwAmNndD9avOizqKLk5uSWsz4PvNF7rvOz4o/U+MSWP566L4S5m9O77hTQ3D4e7D/JvNehpTmIAAgthAoaOgwi04ufK7Zgad9o9tAyOdgeBscvQMOfds8fgtV81oTmp2XmLn8TV3uys7nzNyHgZ+YmXyHv6fHeuUos/KDAqA8dU2m+du+yWyR3XIqzFtpVqdpWmLepqtveXJ9vxzkxs324FNDT27JtQ+OPwEjD5uKP/RA7e5I23KaWbW39QxoWW3eOGxedWL5s/oWqG8z3Z3cyJPXaQQmDpn3G8YeN9dqeJsZo8kNZ/0bhUcBIBKxCgIg3qcAIqIAEInZXAGgacEiIamg+Q9qAYhErZwAUCtAJAQV3v1BLQCRqJUbAGoFiPisirs/qAUgErVKAkCtABEfVXn3B7UARKKmABCJmAJAJGKVBoDGAUR8YtH/B7UARKJWTQCoFSDiA8u7P6gFIBK1agNArQCRLDm4+4NaACJRswkAtQJEsuDo7g9qAYhEzTYA1AoQSZPDuz+oBSAStbmWBS/XyYVoCXERtxzf/UEtAJGouWoBFGgjERHXio+tWd/9wX0LQIOCIi4Vb/Y7qfygLoBI1JIIALUCRFxIYNBvJrUARCKWVACoFSBiI4W7P6gFIBI1148BZ9IEIZFKpXT3B7UARKKWdAugQBOEROaS4ISfUtJqAWhQUGQ2CU/4KUVdAJGIpRkAagWIFJPioN9M2bYAFAIimUo7AE7u1ygEJGYZ3v0h6xZAgUJAYuTB9z6rADg55Ty4GCKpyeCRXzFZtgBS/2VFPJZJfUhrItBsNElI4uHJnb/AhzEADQpKHDyr/OBHAJxMISBxyLwb7EsA6PGg1LaMH/eV4ksAiEgGfBgEnEmDglI7POz3T+djAIAWEpFa4Hnlh5C6ABoTkJAEUPnB3wAo/i60QkBCEEjlB3+7ANOpOyBhCKjiF4QQAFAsBEBBIP4o3Tr1OgB87QLMVPwiqksgPgi08kM4AQAKAfFRwJUfwgoAUAiIT0rP7gui8kN4AQB6QiA+CPzOXxDKIGApekIg6aqRil8QegCAnhBIGmZvYQZZ+aE2AgBKhQAoCMRejVZ+CHMMoJig/wgSrOC/d7XSAihQd0DcKT2zL08NVH6ovQCYTgOEUp0Ap/RWK64AKFAQSDE13NcvpZYDoEBBILObew5JTVZ+iCMAYLYQAAVBzCK860/XmPUJpKTwh4wi7cRazVf8glhaADNpgDB2EQ30zSbWAChQEMRGFf8pFAClKAhqS+R9/VJiD4ACBUGtiniEvxwKgBP0pKCWqOKXRQFwMgVByFTxK6IAKK28C6NAyFb5C8Go4hehAJibgsBHqvhOKADKpyDwgSq+UwqAypV/wRQG7qjiJ0IBYE+B4FplC7yqwltQALijILClip86BYB7lV/QWAOhuqXcVfEdUgAkq7qLW6uBYLd3gyp+AhQA6ar+YocWCqrsQVAAZMPdRc86GNzuyKSKnzIFgB+y+SMUwiO7bdVU4TOmAPBTrf5RVOE9E8uSYKEpVlFCCwVV9gAoAMJRqkJlHQyq6AFTAITPpgIWwkOVOFL/D7oG0KOUhNs5AAAAAElFTkSuQmCC')"></span>
        <span class="ml-2 d-none d-lg-block">
          <span class="text-default">System Owner</span>
          <small class="text-muted d-block mt-1">info@example.com</small>
        </span>
      </a>
      <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
        <a class="dropdown-item" href="https://demo.businessmanager.app/profile">
          <i class="dropdown-icon fe fe-user"></i> Profile
        </a>
        <a class="dropdown-item" href="https://demo.businessmanager.app/settings">
          <i class="dropdown-icon fe fe-settings"></i> Settings
        </a>
        <div class="dropdown-divider"></div>
        <a class="dropdown-item" href="https://demo.businessmanager.app/uploads">
          <i class="dropdown-icon fe fe-folder"></i> Uploads
        </a>
        <div class="dropdown-divider"></div>
        <a class="dropdown-item" href="https://demo.businessmanager.app/logout">
          <i class="dropdown-icon fe fe-log-out"></i> Sign out
        </a>
      </div>
    </div>
  </div>
  <a href="#" class="header-toggler d-lg-none ml-3 ml-lg-0" data-toggle="collapse" data-target="#headerMenuCollapse">
    <span class="header-toggler-icon"></span>
  </a>
</div>
</div>
</div>
            <div class="header collapse d-lg-flex p-0" id="headerMenuCollapse">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg order-lg-first">
                            <ul class="nav nav-tabs border-0 flex-column flex-lg-row">
                                <li class="nav-item">
                                    <a href="<?php echo e(route('dashboard')); ?>" class="nav-link"><i class="fe fe-home"></i> Home</a>
                                </li>

                                <li class="nav-item">
                                    <a href="<?php echo e(route('users.index')); ?>" class="nav-link"><i class="fe fe-home"></i> Home</a>
                                </li>

                    
                                <!--
                                <li class="nav-item">
                                    <a href="<?php echo e(url('/')); ?>/admin/dashboard" class="nav-link"><i class="fe fe-home"></i> Home</a>
                                </li>

                                <li class="nav-item">
                                    <a href="javascript:void(0)" class="nav-link" data-toggle="dropdown"><i class="fe fe-box"></i> Interface</a>
                                    
                                    <div class="dropdown-menu dropdown-menu-arrow">
                                        <a href="./cards.html" class="dropdown-item ">Cards design</a>
                                        <a href="./charts.html" class="dropdown-item ">Charts</a>
                                        <a href="./pricing-cards.html" class="dropdown-item ">Pricing cards</a>
                                    </div>
                                </li>
                            -->
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="my-3 my-md-5">
                <div class="container">
                    <?php echo $__env->yieldContent('content'); ?>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>
                