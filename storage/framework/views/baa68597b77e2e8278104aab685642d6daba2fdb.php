<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                  <h3 class="card-title"><?php echo e($user->name); ?> - <?php echo e($user->firstname); ?> <?php echo e($user->lastname); ?></h3>
                </div>
    
                <div class="card-body p-0 px-3">
                    <div class="tab-content">                    
                        <div class="card-body px-3 pt-1 pb-0 mb-0">
                            <div class="row">
                                <div class="col">
                                    <legend class="pt-5 pb-3">Gebruiker</legend>

                                    <div class="form-group">
                                        <label class="control-label required"><?php echo e(trans('attribute.user.name')); ?></label>
                                        <input class="form-control" minlength="2" maxlength="32" value="<?php echo e($user->name); ?>" required disabled>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label required"><?php echo e(trans('attribute.user.email')); ?></label>
                                        <input class="form-control" minlength="2" maxlength="32" value="<?php echo e($user->email); ?>" required disabled>
                                    </div>
                                </div>
                                
                                <div class="col">
                                    <legend class="pt-5 pb-3">Persoonlijk</legend>
                                    
                                    <div class="form-group">
                                         <div class="form-group">
                                            <label class="control-label required"><?php echo e(trans('attribute.user.firstname')); ?></label>
                                            <input class="form-control" minlength="2" maxlength="32" value="<?php echo e($user->firstname); ?>" required disabled>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                         <div class="form-group">
                                            <label class="control-label required"><?php echo e(trans('attribute.user.lastname')); ?></label>
                                            <input class="form-control" minlength="2" maxlength="32" value="<?php echo e($user->lastname); ?>" required disabled>
                                        </div>
                                    </div>
                                </div>

                                <div class="col">
                                    <legend class="pt-5 pb-3">Rechten</legend>
                                    
                                    <div class="table-responsive">
                                        <table class="table table-outline table-vcenter text-nowrap card-table">
                                            <tbody>
                                                <?php $__currentLoopData = $permissions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $permission): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <tr>
                                                        <td><?php echo e(str_replace('.', ' ', $permission->name)); ?></td>
                                                    </tr>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="col">
                                    <legend class="pt-5 pb-3">Social media</legend>
                                    
                                    <div class="form-group">
                                        <a href="https://twitter.com/<?php echo e($user->social_twitter); ?>" target="_blank" class="btn btn-azure btn-block <?php if($user->social_twitter === '#'): ?> disabled <?php endif; ?>">Twitter</a>
                                    </div>
                                                    
                                    <div class="form-group">
                                        <a href="https://facebook.com/<?php echo e($user->social_facebook); ?>" target="_blank" class="btn btn-blue btn-block <?php if($user->social_facebook === '#'): ?> disabled <?php endif; ?>">Facebook</a>
                                    </div>

                                    <div class="form-group">
                                        <a href="https://instagram.com/<?php echo e($user->social_instagram); ?>" target="_blank" class="btn btn-purple btn-block <?php if($user->social_instagram === '#'): ?> disabled <?php endif; ?>">Instagram</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="card-footer text-right">
                            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit.user')): ?>
                                <a href="<?php echo e(route('user.edit', [$user->id])); ?>" class="btn btn-secondary">Bewerk gebruiker</a>
                            <?php endif; ?>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
<?php $__env->stopSection(); ?>
<?php echo $__env->make('parts.header', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>