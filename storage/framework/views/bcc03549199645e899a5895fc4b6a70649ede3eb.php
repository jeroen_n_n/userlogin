<!DOCTYPE html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <title><?php echo e(config('app.name')); ?> - <?php echo $__env->yieldContent('pageTitle'); ?></title>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>


    <script src="<?php echo e(asset('js/require.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/dashboard.js')); ?>"></script>
    <link href="<?php echo e(asset('css/app.css')); ?>" rel="stylesheet">

    <script>
      requirejs.config({
          baseUrl: '././'
      });
    </script>
    
    <link href="<?php echo e(asset('plugins/charts-c3/plugin.css')); ?>" rel="stylesheet" />
    <script src="<?php echo e(asset('plugins/charts-c3/plugin.js')); ?>"></script>

    <link href="<?php echo e(asset('plugins/maps-google/plugin.css')); ?>" rel="stylesheet" />
    <script src="<?php echo e(asset('plugins/maps-google/plugin.js')); ?>"></script>

    <script src="<?php echo e(asset('plugins/input-mask/plugin.js')); ?>"></script>
</head>

<body>
    <div class="page">
        <div class="page-main">
            <div class="header py-4">
                <div class="container">
                    <div class="d-flex">
                        <a class="header-brand" href="<?php echo e(url('/')); ?>/admin/dashboard">
                            <div style="transform: scale(1.17); width: 39px;">
                                <svg width="39" height="50"><defs></defs>
                                    <g featurekey="root" fill="#ffffff"></g>
                                    <g featurekey="text1" fill="#000000" transform="matrix(0.4,0,0,0.4,20,10)"><path d=""></path></g>
                                    <g featurekey="text3" fill="#000000" transform="matrix(1.3736264240106906,0,0,1.3736264240106906,5,17.47252782521769)"><path d="M0.08 18.86 c-0.08 -0.14 -0.08 -0.16 -0.08 -0.18 c0 -0.08 0.08 -0.16 0.16 -0.24 l0.7 -0.66 c0.06 -0.06 0.12 -0.1 0.18 -0.1 s0.1 0.02 0.16 0.06 c0.6 0.4 0.8 0.68 1.56 0.68 c0.82 0 1.76 -0.66 1.76 -1.78 l0 -10.86 c0 -0.14 0.06 -0.26 0.28 -0.3 l1.06 0 c0.22 0 0.32 0.16 0.32 0.32 l0 10.84 c0 2.08 -1.7 3.4 -3.42 3.4 c-1.06 0 -2.06 -0.36 -2.68 -1.18 z M20.881 5.66 l0.1 0 c0.3 0 0.38 0.14 0.38 0.32 l-0.1 13.72 c0 0.2 -0.12 0.3 -0.36 0.3 l-1.28 0 l-8.88 -12 l0 7.44 l3.5 0.02 c0.18 0 0.3 0.14 0.3 0.32 l0 1.02 c0 0.18 -0.12 0.34 -0.32 0.34 l-4.84 -0.02 c-0.2 0 -0.3 -0.1 -0.3 -0.26 l0 -10.94 c0 -0.18 0.14 -0.32 0.34 -0.32 l1.38 0 c0.14 0 0.24 0.06 0.32 0.16 l8.5 11.46 l0.08 -11.26 c0 -0.16 0.14 -0.3 0.3 -0.3 l0.88 0 z M15.981 18.32 c0.18 0 0.3 0.14 0.3 0.32 l0 1.02 c0 0.18 -0.14 0.34 -0.32 0.34 l-6.58 0 c-0.18 0 -0.3 -0.1 -0.3 -0.3 l0 -1.02 c0 -0.2 0.1 -0.36 0.32 -0.36 l6.58 0 z"></path></g>
                                </svg>
                            </div>
                        </a>
                        
                        <div class="d-flex order-lg-2 ml-auto">
                            <span class="ml-2 d-none d-lg-block">
                              <span class="text-default"> <?php echo e(Auth::user()->name); ?></span>
                              <small class="text-muted d-block mt-1"><?php echo e(Auth::user()->email); ?></small>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="header collapse d-lg-flex p-0" id="headerMenuCollapse">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-3 ml-auto">
                    </div>

                    <div class="col-lg order-lg-first">
                        <ul class="nav nav-tabs border-0 flex-column flex-lg-row">
                            <li class="nav-item">
                                <a href="<?php echo e(route('dashboard')); ?>" class="nav-link"><i class="fe fe-home"></i> Home</a>
                            </li>

                            <li class="nav-item">
                                <a href="<?php echo e(route('users.index')); ?>" class="nav-link"><i class="fe fe-home"></i> Home</a>
                            </li>

                   
                            <!--
                            <li class="nav-item">
                                <a href="<?php echo e(url('/')); ?>/admin/dashboard" class="nav-link"><i class="fe fe-home"></i> Home</a>
                            </li>

                            <li class="nav-item">
                                <a href="javascript:void(0)" class="nav-link" data-toggle="dropdown"><i class="fe fe-box"></i> Interface</a>
                                
                                <div class="dropdown-menu dropdown-menu-arrow">
                                    <a href="./cards.html" class="dropdown-item ">Cards design</a>
                                    <a href="./charts.html" class="dropdown-item ">Charts</a>
                                    <a href="./pricing-cards.html" class="dropdown-item ">Pricing cards</a>
                                </div>
                            </li>
                        -->
                        </ul>
                    </div>
                </div>
            </div>
        </div>
          
        <div class="my-3 my-md-5">
            <div class="container">
                <?php echo $__env->yieldContent('content'); ?>
            </div>
        </div>
    </div>
</div>

</body>
</html>
                