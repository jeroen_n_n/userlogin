<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col">
            <?php if($errors->first()): ?>
                <div class="alert alert-icon alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert"></button>
                    <i class="fe fe-alert-triangle mr-2" aria-hidden="true"></i> <?php echo e($errors->first()); ?>

                </div>
            <?php endif; ?>

            <?php if(Session::has('success')): ?>
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert"></button>
                    <?php echo e(Session::get('success')); ?>

                </div>
            <?php endif; ?>
        </div>
    </div>

    <div class="row">
        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('create.users')): ?>
            <div class="col-sm-4">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Maak een nieuw account</h3>
                    </div>

                    <div class="card-body">
                        <form action="<?php echo e(route('users.create')); ?>" method="post">
                            <?php echo method_field('post'); ?>
                            <?php echo e(csrf_field()); ?>

                            
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label class="form-label required"><?php echo e(trans('attribute.user.firstname')); ?></label>
                                        <input type="text" name="firstname" class="form-control <?php if($errors->has('firstname')): ?> is-invalid <?php endif; ?>" minlength="2" maxlength="32" autocomplete="off" required/>
                                    </div>
                                </div>
                                
                                <div class="col">
                                    <div class="form-group">
                                        <label class="form-label required"><?php echo e(trans('attribute.user.lastname')); ?></label>
                                        <input type="text" name="lastname" class="form-control <?php if($errors->has('lastname')): ?> is-invalid <?php endif; ?>" minlength="2" maxlength="32" autocomplete="off" required/>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="form-label required"><?php echo e(trans('attribute.user.password')); ?></label>
                                <input type="password" name="password" class="form-control <?php if($errors->has('password')): ?> is-invalid <?php endif; ?>" minlength="2" maxlength="32" autocomplete="off" required/>
                            </div>

                            <div class="form-group">
                                <label class="form-label required"><?php echo e(trans('attribute.user.password_confirmation')); ?></label>
                                <input type="password" name="password_confirmation" class="form-control <?php if($errors->has('password_confirmation')): ?> is-invalid <?php endif; ?>"  minlength="2" maxlength="32" autocomplete="off" required/>
                            </div>
                        </div>
                        
                        <div class="modal-footer">
                            <button class="btn btn-success"><?php echo e(trans('web.submit.add_date')); ?></button>
                        </div>
                    </form>
                </div>
            </div>
        <?php endif; ?>

        <div class="col">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Overzicht gebruikers (<?php echo e($users->total()); ?>)</h3>
                </div>

                <div class="table-responsive">
                    <table class="table table-hover table-outline table-vcenter text-nowrap card-table">
                        <thead>
                            <tr>
                                <th class="text-center w-1"></th>
                                <th><?php echo e(trans('attribute.user.name')); ?></th>
                                <th><?php echo e(trans('attribute.user.rank')); ?></th>
                                <th><?php echo e(trans('attribute.user.created_at')); ?></th>

                                <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit.user')): ?>
                                    <th class="text-center"></th>
                                <?php endif; ?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td class="text-center">
                                        <button type="button" class="btn btn-sm btn-icon btn-primary"><?php echo e($user->id); ?></button>
                                    </td>
                                    
                                    <td>
                                        <div><?php echo e($user->name); ?></div>
                                        <div class="small text-muted">
                                            <?php echo e($user->firstname); ?> <?php echo e($user->lastname); ?>

                                        </div>
                                    </td>
                                    
                                    <td>
                                        <select class="form-control">
                                            <?php $__currentLoopData = $user->getRoleNames(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $role): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option><?php echo e(ucfirst($role)); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                    </td>

                                    <td>
                                        <?php echo e($user->created_at); ?>

                                    </td>
                                    
                                    <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit.user')): ?>
                                        <td class="text-center">
                                            <form action="<?php echo e(route('user.destroy', [$user->id])); ?>" method="post">
                                                <?php echo method_field('delete'); ?>
                                                <?php echo e(csrf_field()); ?>


                                                <a href="<?php echo e(route('user.show', $user->id)); ?>" class="btn btn-info">Bekijk gebruiker</button>
                                            </form>
                                        </td>
                                    <?php endif; ?>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>
                </div>

                <div class="modal-footer">
                    <?php echo e($users->links()); ?>

                </div>
            </div>
            <!--
            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('delete.users')): ?>
                <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="modal fade" id="user_<?php echo e($user->id); ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title"><?php echo e(trans('stitle.delete')); ?></h5>
                            </div>

                            <div class="modal-body">
                                <?php echo str_replace('%user', $user->name, trans('smessage.confirm_delete')); ?>

                            </div>

                            <div class="modal-footer">
                                <form action="<?php echo e(route('users.destroy', [$user->id])); ?>" method="post">
                                    <?php echo method_field('delete'); ?>
                                    <?php echo e(csrf_field()); ?>

                                    
                                    <button class="btn btn-danger"><?php echo e(trans('web.submit.delete.user')); ?></button>
                                </form>

                                <button type="button" class="btn btn-success" data-dismiss="modal"><?php echo e(trans('web.submit.close.alert')); ?></button>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php endif; ?>
        </div>
    </div>
-->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('parts.header', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>