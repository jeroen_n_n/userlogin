<!DOCTYPE html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <title><?php echo e(config('app.name')); ?></title>

    <script src="<?php echo e(asset('js/app.js')); ?>" defer></script>
    <link href="<?php echo e(asset('css/app.css')); ?>" rel="stylesheet">
</head>

<body>
    <div class="page">
        <div class="page-single">
            <div class="container">
                <?php if($errors->first()): ?>
                    <div class="alert alert-icon alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert"></button>
                        <i class="fe fe-alert-triangle mr-2" aria-hidden="true"></i> <?php echo e($errors->first()); ?>

                    </div>
                <?php endif; ?>

                <?php if(Session::has('success')): ?>
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert"></button>
                        <?php echo e(Session::get('success')); ?>

                    </div>
                <?php endif; ?>

                <div class="row">
                    <div class="col col-login mx-auto card">
                        <form method="post">
                            <?php echo csrf_field(); ?>

                            <div class="card-body p-6">
                                <div class="card-title">Inlog in uw account</div>
                                <div class="alert alert-warning rounded-0">
                                    <b>Let op:</b> herhaaldelijk foutief inloggen kan leiden tot een tijdelijke blokkade.
                                </div>

                                <div class="form-group">
                                    <input type="text" name="name" class="form-control" placeholder="<?php echo app('translator')->getFromJson('attribute.user.name'); ?>" autocomplete="off" required>
                                </div>
                                
                                <div class="form-group">
                                    <input type="password" name="password" class="form-control" placeholder="<?php echo app('translator')->getFromJson('attribute.user.password'); ?>" autocomplete="off" required>
                                </div>

                                <div class="form-group">
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" name="remember_token" class="custom-control-input" />
                                        <span class="custom-control-label">Onthoud mij</span>
                                    </label>
                                </div>

                                <div class="form-footer">
                                    <button type="submit" class="btn btn-primary btn-block">Login</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>         
</body>
</html>