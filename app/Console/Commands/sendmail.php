<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Mail;
use App\User;
use App\Date;

class sendmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:mail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::All();
        
 
        foreach($users as $user) 
        {
            $date = new Date ();
            $months = $date->getMonths();

            Mail::send('emails.task', ['months' => $months, 'user' => $user, 'total_hours' => $date->getTotalAll()], function ($mail) use ($user) {
                
                $mail->to($user->email)
                    ->from('contact@jeroen.tech', 'Behaalde uren')
                    ->subject('Behaalde uren');
            });
        }
    }
}
