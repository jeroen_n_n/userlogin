<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize ()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules ()
    {
        return [
            'name' => 'min:2|max:32|string|required',
            'firstname' => 'min:2|max:32|string|required',
            'lastname' => 'min:2|max:32|string|required',
            'email' => 'email|min:2|max:32|required',
            'social_twitter' => 'min:1|max:32|string',
            'social_facebook' => 'min:1|max:32|string',
            'social_instagram' => 'min:1|max:32|string',            
            'password' => 'confirmed|max:32|required',
        ];
    }

    public function attributes ()
    {
        return [
            'name' => strtolower(trans('project.attribute.name')),
            'description' => strtolower(trans('project.attribute.description')),
            'active' => strtolower(trans('project.attribute.active')),
            'status' => strtolower(trans('project.attribute.status')),
        ];
    }
}
