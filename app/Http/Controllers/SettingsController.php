<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdatePersonalRequest;
use App\Http\Requests\UpdatePasswordRequest;

use App\Repositories\UserRepository;
use App\User;

use Session;
use Auth;
use Hash;

class SettingsController extends Controller
{

    /**
     * Index()
     * Show the view of settings;
     *
     * @return view;
    */
    public function index ()
    {
        return view('settings.index');
    }


    /**
     * update_personal(UpdatePersonalRequest $request)
     * Update personal data for the user;
     *
     * @param UpdatePersonalRequest $request;
     * @return view;
    */
    public function update_personal (UpdatePersonalRequest $request) 
    {
        // Take the repository for the user to make this easy;
        $user = new UserRepository;

        // The user has been updated;
        if($user->update($request)) {
            Session::flash('success', trans('message.settings.success'));
        }

        return back();
    }


    public function update_password (UpdatePasswordRequest $request) 
    {
        // Take the errors;
        $errors = new \Illuminate\Support\MessageBag();
        
        if (!Hash::check($request->password, Auth::User()->password)) {
            $errors->add(null, trans('message.settings.current_password_failed'));
        }

        elseif(Hash::check($request->new_password, Auth::User()->password)) {
            $errors->add(null, trans('message.settings.no_change'));
        }

        else {
            $user = new UserRepository;
            $user->update(['password' => Hash::make($request->password)]);

            Session::flash('success', trans('message.settings.success'));
        }

        return back()->withErrors($errors);
    }
}
